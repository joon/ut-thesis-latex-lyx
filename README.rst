===================
UT-Thesis-LaTeX-LyX
===================

Thesis style and a template (LaTeX and LyX) for the University of Texas at
Austin, original style and LaTeX template by Craig McCluskey.

I ended up using the master LyX file with a bunch of ERTs (Evil Red Text) for
dissertaiton-specific elements. You should be able to just include (input) lyx files
for your chapters and it should work.

Authors
=======

* Joon H. Ro
* Craig McCluskey (original style and LaTeX template, last updated 25 August
  2002)


